let currentDroppable = null;
var corpses;
var pupils;
var dictionary;

document.getElementById("corpse_id").addEventListener("change", onChangeSelectCorpse);
document.getElementById("profile_id").addEventListener("change", onChangeProfileCorpse);
document.getElementById("audience_id").addEventListener("change", onChangeAudienceCorpse);
document.getElementById("corpse_id").addEventListener("change", FilterOnChange);
document.getElementById("profile_id").addEventListener("change", FilterOnChange);
document.getElementById("audience_id").addEventListener("change", FilterOnChange);

document.getElementById("mainTable").addEventListener("mousedown", function(e) {
    SetStyleForCalcTable(true);
    var x = document.createElement("TABLE");
    x.style.background = "white";
    x.style.opacity = 0.75;
    x.setAttribute("id", "myTable");
    x.style.position = 'absolute';
    document.body.appendChild(x);
    document.getElementById("myTable").appendChild(e.target.parentNode.cloneNode(true));

    x.style.position = 'absolute';
    MoveAt(e);
    document.body.appendChild(x);
    x.style.zIndex = 1000;


    function MoveAt(e) 
    {
      x.style.left = e.pageX - x.offsetWidth / 2 + 'px';
      x.style.top  = e.pageY - x.offsetHeight / 2 + 'px';
    }
  
    document.onmousemove = function(e) 
    {
        MoveAt(e);

        x.hidden = true;
        let elemBelow = document.elementFromPoint(e.clientX, e.clientY);
        x.hidden = false;

        if (!elemBelow) return;

        let droppableBelow = elemBelow.closest('.droppable');
        if (currentDroppable != droppableBelow) 
        {
            if (currentDroppable) { // null when we were not over a droppable before this event
                x.style.cursor = "auto";
                LeaveDroppable(currentDroppable);
            }
            currentDroppable = droppableBelow;
            if (currentDroppable) 
            { 
                x.style.cursor = "copy";
                EnterDroppable(currentDroppable);
            }
        }
    }
    
    x.onmouseup = function() 
    {
        document.onmousemove = null;
        x.onmouseup = null;
        document.getElementById("myTable").outerHTML = "";
        if(currentDroppable!=null)
        {
            if(currentDroppable.className=="droppable")
            {
                var stat_row = {};
                var pupil_row = {};
                var audience_id;
                stat_row["audience"] = currentDroppable.getElementsByTagName("td")[0].innerHTML;
                stat_row["count"] = currentDroppable.getElementsByTagName("td")[1].innerHTML;
                stat_row["max"] = currentDroppable.getElementsByTagName("td")[2].innerHTML;
                if(currentDroppable.getElementsByTagName("td")[3].innerHTML == "")
                    stat_row["bel"] = false;
                else
                    stat_row["bel"] = true;
        
                pupil_row["name"] = e.target.parentNode.getElementsByTagName("td")[1].innerHTML;
                pupil_row["audience"] = e.target.parentNode.getElementsByTagName("td")[2].innerHTML;
                pupil_row["profile"] = e.target.parentNode.getElementsByTagName("td")[3].innerHTML;
        
                if(e.target.parentNode.getElementsByTagName("td")[4].innerHTML == "")
                    pupil_row["bel"] = false;
                else
                    pupil_row["bel"] = true;
                for(var name in dictionary.audiences) {
                    if(dictionary.audiences[name] == stat_row["audience"])
                        audience_id = name;
                }
                if(pupil_row["audience"]!=stat_row["audience"])
                {
                    if(stat_row["bel"]!=pupil_row["bel"])
                    {
                        alert("Несоответствие языков!");
                        //break;
                    }
                    else
                    {
                        corpses.forEach(corpse => {
                            corpse["places"].forEach(place => {
                                place["audience"]
        
                                for(var i=0; i<place["audience"].length; i++)
                                {
                                    if(place["audience"][i]["name"] == stat_row["audience"] && place["audience"][i]["bel"]==stat_row["bel"])
                                    {
                                        place["audience"][i]["count"]++;
                                        for(var n=0; n<place["audience"].length; n++)
                                        {
                                            if(place["audience"][n]["name"] == pupil_row["audience"] && place["audience"][n]["bel"]==pupil_row["bel"])
                                            {
                                                place["audience"][n]["count"]--;
                                            }
                                        }
        
                                        pupils.forEach(pupil => {
                                            if(pupil["_id"]==e.target.parentNode.id)
                                            {
                                                pupil["audience"]=audience_id;
                                            }
                                        });
                                    }
                                }
                            });
                        });
                    }
                }
                DrawMainTable();
                DrawCalcTable();
                SetStyleForCalcTable(false);
            }
        }
    }

    x.ondragstart = function() {
      console.log("start");
        return false;
      };
  });

GetData();

function Topnav_button() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}

function onChangeSelectCorpse()
{
    document.getElementById("profile_id").selectedIndex = 0;
    document.getElementById("audience_id").selectedIndex = 0;
}

function onChangeProfileCorpse()
{
    document.getElementById("audience_id").selectedIndex = 0;
}

function onChangeAudienceCorpse()
{
}

function GetData(){    
    let promise_dictionary=new Promise( (resolve) => 
    {
        $.getJSON("http://lyceumexams.herokuapp.com/api/dictionary")
        .then(function (data) 
            {
                resolve(data); 
            }
        );
    });
    let promise_pupils=new Promise( (resolve) => 
    {
        $.getJSON("http://lyceumexams.herokuapp.com/api/pupils")
        .then(function (data) 
            {
                resolve(data); 
            }
        );
    });
    let promise_corpses=new Promise( (resolve) => 
    {
        $.getJSON("http://lyceumexams.herokuapp.com/api/corpses")
        .then(function (data) 
            {
                resolve(data); 
            }
        );
    });

    Promise.all([promise_dictionary, promise_pupils, promise_corpses]).then(data=>{

        dictionary = data[0];
        pupils = data[1];
        corpses = data[2];
        
  FilterOnChange();
    });
}

function EnterDroppable(elem) {
    if(elem.className != "overflow droppable")
        elem.style.background = 'lightblue';
    
}

function LeaveDroppable(elem) {
    if(elem.className != "overflow droppable")
        elem.style.background = '#eee';
}

function FilterOnChange()
{
    console.log(dictionary);
    console.log(pupils);
    console.log(corpses);
    var selected_copses = document.getElementById("corpse_id").selectedIndex;
    var selected_profile = document.getElementById("profile_id").selectedIndex;
    var selected_audience = document.getElementById("audience_id").selectedIndex;
    if (selected_copses == -1)
    {
        selected_copses = selected_profile =  selected_audience = 0;
    }
    DrawSelectCorpses(selected_copses, corpses);
    DrawSelectProfile(selected_profile, corpses[document.getElementById("corpse_id").selectedIndex]);
    DrawSelectAudience(selected_audience, corpses[document.getElementById("corpse_id").selectedIndex]);

    

    DrawMainTable();
    DrawCalcTable();
}

function DrawSelectCorpses(ind, array)
{
    var corpse_select = document.getElementById("corpse_id");
    corpse_select.options.length = 0;
    array.forEach(corpse => {
        corpse_select.options[corpse_select.options.length] = new Option(corpse.name, corpse.alias);
    });
    corpse_select.selectedIndex = ind;
}

function DrawSelectProfile(ind, array)
{
    var profile_select = document.getElementById("profile_id");
    profile_select.options.length = 0;
    profile_select.options[profile_select.options.length] = new Option("Все", 1);
    array["places"].forEach(place => {
        profile_select.options[profile_select.options.length] = new Option(place["code"], place["_id"]);
    });
    profile_select.selectedIndex = ind;
}

function DrawSelectAudience(ind, array)
{
    var audience_select = document.getElementById("audience_id");
    audience_select.options.length = 0;
    audience_select.options[audience_select.options.length] = new Option("Все", 1);


        if(document.getElementById("profile_id").options[document.getElementById("profile_id").selectedIndex].value == 1)
        {   
            array["places"].forEach(place => {
                place["audience"].forEach(audience => {
                    audience_select.options[audience_select.options.length] = new Option(audience["name"], audience["_id"]);
                });
            });
        }   
        else
        {   
            array["places"].forEach(place => {
                if(document.getElementById("profile_id").options[document.getElementById("profile_id").selectedIndex].value == place["_id"])
                {
                    place["audience"].forEach(audience => {audience_select.options[audience_select.options.length] = new Option(audience["name"], audience["_id"]);
                    });
                }
            });
        }   
    audience_select.selectedIndex = ind;
}

function DrawMainTable() 
{
    var array = [];
    var index = 0;

    ClearTable("mainTable");

    pupils.forEach(pupil => {
        if(
            (pupil["corps"] == document.getElementById("corpse_id").options[document.getElementById("corpse_id").selectedIndex].value) &&
            (document.getElementById("profile_id").options[document.getElementById("profile_id").selectedIndex].value == pupil["place"]||
                document.getElementById("profile_id").options[document.getElementById("profile_id").selectedIndex].value == 1)&&
            (document.getElementById("audience_id").options[document.getElementById("audience_id").selectedIndex].value == pupil["audience"] ||
                document.getElementById("audience_id").options[document.getElementById("audience_id").selectedIndex].value == 1)
            )
        {
        var temp_pupil = {};
        temp_pupil.name = pupil["firstName"] + " " + pupil["lastName"] + " " + pupil["parentName"];
        temp_pupil.profile = dictionary["profiles"][pupil["profile"]];
        temp_pupil.cab =  dictionary["audiences"][pupil["audience"]];
        temp_pupil.bel = pupil["needBel"];
        temp_pupil.id = pupil["_id"];
        array[index++] = temp_pupil;
        }
    });
    

    var table = document.getElementById("mainTable");
    for(var i=0; i<array.length; i++)
    {
        var rows_count = document.getElementById('mainTable').rows.length;
        var row = table.insertRow(rows_count);
        row.insertCell(0).innerHTML = i+1;
        row.insertCell(1).innerHTML = array[i].name;
        row.insertCell(2).innerHTML = array[i].cab;
        row.insertCell(3).innerHTML = array[i].profile;
        if(array[i].bel == false)
            row.insertCell(4).innerHTML = "";
        else
            row.insertCell(4).innerHTML ='<img src="https://lipis.github.io/flag-icon-css/flags/4x3/by.svg" alt="bel" style=height:16px;>';
        row.setAttribute("id", array[i].id);
    }
}

function DrawCalcTable()
{
    ClearTable("calcTable");
    var index = 0;
    var array = [];
    if(document.getElementById("profile_id").options[document.getElementById("profile_id").selectedIndex].value == 1)
    {   
        corpses[document.getElementById("corpse_id").selectedIndex]["places"].forEach(place => {
            place["audience"].forEach(audience => {
                array[index++] = { name: audience["name"], max: audience["max"], count: audience["count"], bel:audience["bel"]};
            });
        });
    }   
    else
    {   
        corpses[document.getElementById("corpse_id").selectedIndex]["places"].forEach(place => {
            if(document.getElementById("profile_id").options[document.getElementById("profile_id").selectedIndex].value == place["_id"])
            {
                place["audience"].forEach(audience => {
                    array[index++] = { name: audience["name"], max: audience["max"], count: audience["count"], bel:audience["bel"]};
                });
            }
        });
    }   

    var calc_table = document.getElementById("calcTable");
    for(var i = 0; i<array.length; i++)
    {
        var rows_count_calc = calc_table.rows.length;
        var row_calc = calc_table.insertRow(rows_count_calc);
        row_calc.insertCell(0).innerHTML = array[i].name;
        row_calc.insertCell(1).innerHTML = array[i].count;
        row_calc.insertCell(2).innerHTML = array[i].max;
        if(array[i].bel == false)
            row_calc.insertCell(3).innerHTML = "";
        else
            row_calc.insertCell(3).innerHTML ='<img src="https://lipis.github.io/flag-icon-css/flags/4x3/by.svg" alt="bel" style=height:16px;>';
        if(array[i].count>array[i].max)
            {
                row_calc.setAttribute("class", "overflow droppable");
            }
        else
        row_calc.setAttribute("class", "droppable");
    }    
}

function ClearTable(name)
{
    var table = document.getElementById(name);
    while(table.rows.length > 1) {
        table.deleteRow(1);
      }
}

function SetStyleForCalcTable(drags)
{
    var rows = document.getElementById("calcTable").getElementsByTagName("tr");

    for (var i = 1; i < rows.length; i++) { 
        if(drags)
        {
            if(rows[i].className != "overflow droppable")
            {
                rows[i].style.backgroundColor = '#eee';  
            }
        }
        else
        if(rows[i].className != "overflow droppable")
        {
            rows[i].style.backgroundColor = 'white';
        }
    }
}

function sortTable(n) 
{
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("mainTable");
    switching = true;
    dir = "asc"; 
    while (switching) 
    {
      switching = false;
      rows = table.getElementsByTagName("TR");
      for (i = 1; i < (rows.length - 1); i++) 
      {
        shouldSwitch = false;
        x = rows[i].getElementsByTagName("TD")[n];
        y = rows[i + 1].getElementsByTagName("TD")[n];
        if (dir == "asc") {
          if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) 
          {
            shouldSwitch= true;
            break;
          }
        } else if (dir == "desc") {
          if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) 
          {
            shouldSwitch = true;
            break;
          }
        }
      }
      if (shouldSwitch) 
      {
        rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
        switching = true;
        switchcount ++;      
      } 
      else 
      {
        if (switchcount == 0 && dir == "asc") {
          dir = "desc";
          switching = true;
        }
      }
    }
}