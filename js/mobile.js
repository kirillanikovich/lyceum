var corpses;
var pupils;
var dictionary;


var selected_corpse = {
    id:-1, 
    alias:0
};
var selected_profile = {
    id:0, 
    value:1
};
var selected_audience = {
    id:0, 
    value:1
};

var PupilsInTable = [];


document.getElementById("profile_id").addEventListener("change", onChangeProfile);
document.getElementById("audience_id").addEventListener("change", onChangeAudience);

document.getElementById("mainTable").getElementsByTagName("thead")[0].getElementsByTagName("th")[0].addEventListener("click", sortTable);
document.getElementById("mainTable").getElementsByTagName("thead")[0].getElementsByTagName("th")[1].addEventListener("click", sortTable);

GetData();
OpenNav();

function OpenNav() {
    document.getElementById("mySidenav").style.left = "0";
    document.getElementById("wrap").style.opacity = "0.37";
    document.body.style.backgroundColor = "#222";
}

function CloseNav() {
    document.getElementById("mySidenav").style.left = "-30%";
    document.getElementById("wrap").style.opacity = "1";
    document.body.style.backgroundColor = "white";
}


function GetData(){    
    let promise_dictionary=new Promise( (resolve) => 
    {
        $.getJSON("http://lyceumexams.herokuapp.com/api/dictionary")
        .then(function (data) 
            {
                resolve(data); 
            }
        );
    });
    let promise_pupils=new Promise( (resolve) => 
    {
        $.getJSON("http://lyceumexams.herokuapp.com/api/pupils")
        .then(function (data) 
            {
                resolve(data); 
            }
        );
    });
    let promise_corpses=new Promise( (resolve) => 
    {
        $.getJSON("http://lyceumexams.herokuapp.com/api/corpses")
        .then(function (data) 
            {
                resolve(data); 
            }
        );
    });

    Promise.all([promise_dictionary, promise_pupils, promise_corpses]).then(data=>{

        dictionary = data[0];
        pupils = data[1];
        corpses = data[2];
        var index = 0;
        corpses.forEach(corpse => {
            var corpse_button = document.createElement("button");
            corpse_button.setAttribute("type", "button");
            corpse_button.setAttribute("id", index++);
            corpse_button.setAttribute("class", "list-group-item list-group-item-action");
            corpse_button.innerHTML = corpse["name"];
            corpse_button.addEventListener("click", onChangeCorpse);
            corpse_button.addEventListener("click", FilterOnChange);
            document.getElementById("list-tab").appendChild(corpse_button);
        });
    
        
    document.getElementById("profile_id").options[document.getElementById("profile_id").options.length] = new Option("Все", 1);
    document.getElementById("audience_id").options[document.getElementById("audience_id").options.length] = new Option("Все", 1);

    FilterOnChange(); 
    });
}


function GetPupilByRequest(array)
{
    var index = 0;
    var array = []; 

    pupils.forEach(pupil => {
        if(
            pupil["corps"] == selected_corpse.alias &&
            (selected_profile.value == pupil["place"] || selected_profile.value == 1) &&
            (selected_audience.value == pupil["audience"] || selected_audience.value == 1)
          )
        {
        var temp_pupil = {};
        temp_pupil.name = pupil["firstName"] + " " + pupil["lastName"] + " " + pupil["parentName"];
        temp_pupil.profile = dictionary["profiles"][pupil["profile"]];
        temp_pupil.cab =  dictionary["audiences"][pupil["audience"]];
        temp_pupil.bel = pupil["needBel"];
        temp_pupil.id = pupil["_id"];
        temp_pupil.place_code = dictionary["places"][pupil["place"]]["code"];
        temp_pupil.phone = pupil["phone"];
        temp_pupil.email = pupil["email"];
        array[index++] = temp_pupil;
        }
    });
    return array;
}


function DrawMainTable() 
{

    var array = PupilsInTable;
    ClearTable("mainTable");
    var table = document.getElementById("mainTable").getElementsByTagName('tbody')[0];    
    for(var i=0; i<array.length;i++)
    {
        var row = table.insertRow(i*2);
        row.insertCell(0).innerHTML = array[i].name;
        row.insertCell(1).innerHTML = array[i].cab + " " + 
                                      ((array[i].bel == true) ? "(бел)" : "") + 
                                      "<br>" + array[i].place_code;
        row.insertCell(2).innerHTML = "";
        row.setAttribute("id", array[i].id);
        row.setAttribute("data-toggle", "collapse");
        row.setAttribute("data-target", "#demo" + i);
        row.setAttribute("class", "accordion-toggle");


        var row_collapsed = table.insertRow(i*2+1);
        row_collapsed.setAttribute("class", "hiddenRow");

        var rowcollapsed1 = document.createElement("DIV");
        var rowcollapsed2 = document.createElement("DIV");
        rowcollapsed2.setAttribute("class", "accordian-body collapse additional_div");
        rowcollapsed1.innerHTML = "Профиль: " + array[i].profile  + "<br>" + 
                                  "Телефон: " + '<a href="tel://' + array[i].phone + '">'+ array[i].phone + "</a><br>" + 
                                  "email: " + array[i].email;
        
        var button_inquiry  = document.createElement("button");
        button_inquiry.setAttribute("class", "button_inqury");
        button_inquiry.innerHTML = "&#9786; СПРАВКА";
        rowcollapsed1.appendChild(document.createElement("br"));
        rowcollapsed1.appendChild(button_inquiry);

        rowcollapsed2.setAttribute("id", "demo"+i);
        row_collapsed.insertCell(0).appendChild(rowcollapsed2);
        rowcollapsed2.appendChild(rowcollapsed1);

        row_collapsed.cells[0].colSpan = 3;
    }
    $(".accordian-body").on("show.bs.collapse", function() {
        $(this)
          .closest("table")
          .find(".collapse.show")
          .not(this)
          .collapse("toggle");
    });

}


function ClearTable(name)
{
    var table = document.getElementById(name);
    while(table.rows.length > 1) {
        table.deleteRow(1);
      }
}

function onChangeProfile()
{
    if(document.getElementById("profile_id").selectedIndex != -1)
    {
        selected_profile.id = document.getElementById("profile_id").selectedIndex;
        selected_profile.value = document.getElementById("profile_id").options[selected_profile.id].value;
    }
    document.getElementById("audience_id").selectedIndex = 0;
    selected_audience.id = 0;
    onChangeAudience();
}

function onChangeAudience()
{
    if(document.getElementById("audience_id").selectedIndex != -1)
    {
        selected_audience.id = document.getElementById("audience_id").selectedIndex;
        selected_audience.value = document.getElementById("audience_id").options[selected_audience.id].value;
    }
    selected_audience.id = document.getElementById("audience_id").selectedIndex;
    selected_audience.value = document.getElementById("audience_id").options[selected_audience.id].value;

    FilterOnChange();
}

function onChangeCorpse()
{
    selected_corpse.id = event.target.getAttribute("id");
    selected_corpse.alias = corpses[selected_corpse.id]["alias"];
    CloseNav();
    document.getElementById("navtext").innerHTML = corpses[selected_corpse.id]["name"];
    document.getElementById("profile_id").selectedIndex = 0;
    selected_profile.id = 0;
    onChangeProfile();
}

function DrawSelectProfile(array)
{
    var profile_select = document.getElementById("profile_id");

    profile_select.options.length = 0;
    profile_select.options[profile_select.options.length] = new Option("Все", 1);

    array["places"].forEach(place => {
        profile_select.options[profile_select.options.length] = new Option(place["code"] + " - " + CountInProfile(place["_id"])+ "ч.", place["_id"]) ;
    });
    profile_select.selectedIndex = selected_profile.id;
}

function DrawSelectAudience(array)
{ 
    var audience_select = document.getElementById("audience_id");

    audience_select.options.length = 0;
    audience_select.options[audience_select.options.length] = new Option("Все", 1);

    if(selected_profile.value == 1)
    {   
        array["places"].forEach(place => {
            place["audience"].forEach(audience => {
                audience_select.options[audience_select.options.length] = new Option(audience["name"] + " " + ((audience.bel == true) ? "(бел)" : "")  + " - " + CountInAudiences(audience["_id"]) + "ч.", audience["_id"]);
            });
        });
    }   
    else
    {   
        array["places"].forEach(place => {
            if(selected_profile.value == place["_id"])
            {
                place["audience"].forEach(audience => {
                    audience_select.options[audience_select.options.length] = new Option(audience["name"] + " " + ((audience.bel == true) ? "(бел)" : "")  + " - " + CountInAudiences(audience["_id"]) + "ч.", audience["_id"]);
                });
            } 
        });
    }   
    audience_select.selectedIndex = selected_audience.id;
}

function CountInAudiences(audience)
{
    var index = 0;
    pupils.forEach(pupil => {
        if(pupil["audience"]==audience)
        index++;
    });
    return index;
}

function CountInProfile(place)
{
    var index = 0;
    pupils.forEach(pupil => {
        if(pupil["place"]==place)
        index++;
    });
    return index;
}

function FilterOnChange()
{
    console.log(dictionary);
    console.log(pupils);
    console.log(corpses);

    if(selected_corpse.id > -1)
    {
        DrawSelectProfile(corpses[selected_corpse.id]);    
        DrawSelectAudience(corpses[selected_corpse.id]); 
        PupilsInTable = GetPupilByRequest();
        console.log(PupilsInTable);
        DrawMainTable();
    }
    document.getElementById("1col").removeAttribute("class");
    document.getElementById("2col").removeAttribute("class");
}


function sortTable() 
{
    var n;

    if(event.target.getElementsByTagName("i")[0].getAttribute("id")=="1col")
    {
        n = 0;
    }
    else if(event.target.getElementsByTagName("i")[0].getAttribute("id")=="2col")
    {
        n = 1;
    }

    if(n==0)
    {
        if(event.target.getElementsByTagName("i")[0].getAttribute("class") == null || event.target.getElementsByTagName("i")[0].getAttribute("class") == "fa fa-sort-alpha-desc")
        { 
            PupilsInTable.sort(function(a, b) {
                event.target.getElementsByTagName("i")[0].setAttribute("class", "fa fa-sort-alpha-asc");
                var nameA = a.name.toUpperCase();
                var nameB = b.name.toUpperCase();
                if (nameA < nameB) {
                return -1;
                }
                if (nameA > nameB) {
                return 1;
                }
                return 0;
            });
        }
        else{ 
            PupilsInTable.sort(function(a, b) {
                event.target.getElementsByTagName("i")[0].setAttribute("class", "fa fa-sort-alpha-desc");
                var nameA = a.name.toUpperCase();
                var nameB = b.name.toUpperCase();
                if (nameA > nameB) {
                return -1;
                }
                if (nameA < nameB) {
                return 1;
                }
                return 0;
            });
        }
        document.getElementById("2col").removeAttribute("class");
    }
    if(n==1)
    {
        if(event.target.getElementsByTagName("i")[0].getAttribute("class") == null || event.target.getElementsByTagName("i")[0].getAttribute("class") == "fa fa-sort-alpha-desc")
        { 
            PupilsInTable.sort(function(a, b) {
                event.target.getElementsByTagName("i")[0].setAttribute("class", "fa fa-sort-alpha-asc");
                var cabA = a.cab;
                var cabB = b.cab;
                if (cabA < cabB) {
                return -1;
                }
                if (cabA > cabB) {
                return 1;
                }
                return 0;
            });
        }
        else  
        { 
            PupilsInTable.sort(function(a, b) {
                event.target.getElementsByTagName("i")[0].setAttribute("class", "fa fa-sort-alpha-asc");
                var cabA = a.cab;
                var cabB = b.cab;
                if (cabA > cabB) {
                return -1;
                }
                if (cabA < cabB) {
                return 1;
                }
                return 0;
            });
        }
        document.getElementById("1col").removeAttribute("class");
    }
    DrawMainTable();
}